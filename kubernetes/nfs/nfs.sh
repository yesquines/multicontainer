#!/bin/bash
# Instalando Pacotes para o Loadbalancer e NFS
apt-get install -y vim nfs-kernel-server nfs-common -qq &> /dev/null && \
				echo "OK - Instalando Pacotes - Loadbalancer e NFS"

# Configurando HAProxy
cat > /etc/exports <<EOF
/srv/v1        192.168.99.0/24(rw,sync,no_root_squash)
/srv/v2        192.168.99.0/24(rw,sync,no_root_squash)
/srv/v3        192.168.99.0/24(rw,sync,no_root_squash)
/srv/v4        192.168.99.0/24(rw,sync,no_root_squash)
EOF
echo "OK - Configurado NFS"

# Restart de Serviço
systemctl restart nfs-kernel-server && echo "OK - Serviço Iniciado"

# Configurando Pasta do NFS
mkdir -p /srv/v{1..4} && echo "OK - Configurada Pastas do NFS"
